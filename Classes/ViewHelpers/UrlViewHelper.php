<?php
declare(strict_types=1);
namespace VITD\Assets\ViewHelpers;

use Closure;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Get a public url for a static asset
 */
class UrlViewHelper extends AbstractViewHelper
{
    public function initializeArguments(): void
    {
        $this
            ->registerArgument('path', 'string', 'Asset relative path (below EXT:assets/Resources/Public/)', true)
            ->registerArgument('absolute', 'bool', 'Whether to render the URL absolute (true) or not (false; default)', false, false);
    }


    public static function renderStatic(
        array $arguments,
        Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ): string {
        $path = $arguments['path'];
        $absolute = $arguments['absolute'];

        $url = PathUtility::stripPathSitePrefix(GeneralUtility::getFileAbsFileName('EXT:assets/Resources/Public/' . $path));
        if (TYPO3_MODE === 'BE' && $absolute === false && $url !== false) {
            $url = '../' . $url;
        }
        if ($absolute === true) {
            // controllerContext is deprecated/unsafe, but currently (TYPO3 10 LTS) the f:base viewhelper does the same.
            $url = $renderingContext->getControllerContext()->getRequest()->getBaseUri() . $url;
        }

        return $url;
    }
}
