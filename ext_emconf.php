<?php
$EM_CONF['assets'] = [
    'title' => 'Collection of assets for the current website(s)',
    'description' => 'Assets shared across site package and extensions.',
    'category' => 'misc',
    'author' => 'Ludwig Rafelsberger',
    'author_email' => 'ludwig.rafelsberger@vitd.at',
    'author_company' => 'VITAMIN D GmbH',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '104.0.0',
    'constraints' => [
        'depends' => [
            'core' => '10.4.0-10.4.99',
            'fluid_styled_content' => '10.4.0-10.4.99',
            'frontend' => '10.4.0-10.4.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
