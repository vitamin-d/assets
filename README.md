Assets
======

Hold your website's static assets in this extension. Asset files are excluded from git to avoid duplication (assuming
your continuous integration pipeline does copy them from the prototypes). Concise viewhelper to use the assets in your
Fluid templates is included.


## Features

* Git exclusion of static assets
* ViewHelper to use static assets


## Bugs

These methods have been used in different projects for now and are thoroughly real-life-tested. If there are any
remaining bugs: Please report them.
